package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.entity.Student;
import camt.se234.lab11.exception.NoDataException;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StudentServiceImplTest {
    StudentDao studentDao;
    StudentServiceImpl studentService;

    @Before
    public void setup(){
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
    }

    @Test
    public void testFindById(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findStudentById("123"),is(new Student("123","A","temp",2.33)));
        assertThat(studentService.findStudentById("124"),is(new Student("124","Janie","Bul",3.00)));
        assertThat(studentService.findStudentById("125"),is(new Student("125","Lisa","lalis",3.25)));
        assertThat(studentService.findStudentById("126"),is(new Student("126","Merry","Jons",4.0)));
        assertThat(studentService.findStudentById("127"),is(new Student("127","Mik","Ola",3.65)));
    }

    @Test
    public void testGetAverageGpa(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(),is(3.246));
    }

    @Test
    public void testWithMock(){

        List<Student> mockStudents = new ArrayList<>();

        mockStudents.add(new Student("123", "A", "temp", 2.33));
        mockStudents.add(new Student("124", "B", "temp", 2.33));
        mockStudents.add(new Student("223", "C", "temp", 2.33));
        mockStudents.add(new Student("224", "D", "temp", 2.33));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"),is(new Student("123", "A", "temp", 2.33)));
    }

    @Test
    public void testFindByIdWithMock(){
        setup();
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("502", "JK", "Rio", 2.33));
        mockStudents.add(new Student("503", "Lisa", "Jikam", 3.33));
        mockStudents.add(new Student("254", "Ola", "Paja", 2.99));
        mockStudents.add(new Student("652", "steve", "Lam", 3.85));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("254"),is(new Student("254", "Ola", "Paja", 2.99)));
    }

    @Test
    public void testGetAverageGpaOfFourStudentsWithMock(){
        List<Student> mockStudents = new ArrayList<>();

        mockStudents.add(new Student("502", "JK", "Rio", 2.33));
        mockStudents.add(new Student("503", "Lisa", "Jikam", 3.33));
        mockStudents.add(new Student("254", "Ola", "Paja", 2.99));
        mockStudents.add(new Student("652", "steve", "Lam", 3.85));

        when(studentDao.findAllAverage()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(3.125));
    }

    @Test
    public void testGetAverageGpaOfTwoStudentsWithMock(){
        List<Student> mockStudents = new ArrayList<>();

        mockStudents.add(new Student("502", "JK", "Rio", 2.33));
        mockStudents.add(new Student("503", "Lisa", "Jikam", 3.33));

        when(studentDao.findAllAverage()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(2.83));
    }

    @Test
    public void testGetAverageGpaOfSixStudentsWithMock(){
        List<Student> mockStudents = new ArrayList<>();

        mockStudents.add(new Student("502", "JK", "Rio", 2.33));
        mockStudents.add(new Student("503", "Lisa", "Jikam", 3.33));
        mockStudents.add(new Student("254", "Ola", "Paja", 2.99));
        mockStudents.add(new Student("652", "steve", "Lam", 3.85));
        mockStudents.add(new Student("223", "Thipa", "Danu", 3.89));

        when(studentDao.findAllAverage()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(3.278));
    }

    @Test
    public void testฺัFindByPastOfId(){
        List<Student> mockStudents = new ArrayList<>();

        mockStudents.add(new Student("123", "A", "temp", 2.33));
        mockStudents.add(new Student("124", "B", "temp", 2.33));
        mockStudents.add(new Student("223", "C", "temp", 2.33));
        mockStudents.add(new Student("224", "D", "temp", 2.33));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("22"),hasItem(new Student("223", "C", "temp", 2.33)));
        assertThat(studentService.findStudentByPartOfId("22"),hasItems(new Student("223", "C", "temp", 2.33),new Student("224", "D", "temp", 2.33)));
    }

    @Test
    public void testฺัFindByPastOfIdOfFiveStudents(){
        List<Student> mockStudents = new ArrayList<>();

        mockStudents.add(new Student("502", "JK", "Rio", 2.33));
        mockStudents.add(new Student("503", "Lisa", "Jikam", 3.33));
        mockStudents.add(new Student("254", "Ola", "Paja", 2.99));
        mockStudents.add(new Student("652", "steve", "Lam", 3.85));
        mockStudents.add(new Student("223", "Thipa", "Danu", 3.89));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("50"),hasItem(new Student("502", "JK", "Rio", 2.33)));
        assertThat(studentService.findStudentByPartOfId("50"),hasItems(new Student("502", "JK", "Rio", 2.33),new Student("503", "Lisa", "Jikam", 3.33)));
    }

    @Test(expected = NoDataException.class)
    public void testNoDataException(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123", "A", "temp", 2.33));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("55"),nullValue());
    }

    @Test(expected = NoDataException.class)
    public void testFindByPastOfIdWhenSizeOfOutputArrayIsZero(){
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("502", "JK", "Rio", 2.33));
        mockStudents.add(new Student("503", "Lisa", "Jikam", 3.33));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("55"),hasSize(0));
    }

    @Test(expected = ArithmeticException.class)
    public void testGetAverageGpaWhenDataIsDividedByZero(){
        List<Student> mockStudents = new ArrayList<>();

        when(studentDao.findAllAverage()).thenReturn(mockStudents);
        assertThat(studentService.getAverageGpa(),is(0.0));
    }


}
