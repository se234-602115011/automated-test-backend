package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    List<Student> students;
    public StudentDaoImpl(){
        students = new ArrayList<>();
        students.add(new Student("123","A","temp",2.33));
        students.add(new Student("124","Janie","Bul",3.00));
        students.add(new Student("125","Lisa","lalis",3.25));
        students.add(new Student("126","Merry","Jons",4.0));
        students.add(new Student("127","Mik","Ola",3.65));

    }

    @Override
    public List<Student> findAll() {
        return this.students;
    }

    @Override
    public List<Student> findAllAverage() {
        return this.students;
    }
}
