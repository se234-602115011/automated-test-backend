package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.entity.Student;
import camt.se234.lab11.exception.NoDataException;

import java.util.ArrayList;
import java.util.List;

public class StudentServiceImpl implements StudentService {
    StudentDao studentDao;

    public void setStudentDao(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    @Override
    public Student findStudentById(String id) {
        for (Student student: this.studentDao.findAll()
             ) {
            if (student.getStudentId().equals(id)){
                return student;
            }
        }
        throw new NoDataException();
    }

    @Override
    public List<Student> findStudentByPartOfId(String id) {
        List<Student> output = new ArrayList<>();
        for (Student student: this.studentDao.findAll()) {
            if (student.getStudentId().indexOf(id) != -1){
                output.add(student);
            }
        }
        if(output.size()> 0){
            return output;
        }else{
            throw new NoDataException();
        }

    }

    @Override
    public double getAverageGpa() {
        double total = 0;
        for (Student student: this.studentDao.findAllAverage()
                ) {
            total += student.getGpa();

        }
        if(this.studentDao.findAllAverage().size() > 0){
            return total/this.studentDao.findAllAverage().size();
        }else{
           throw new ArithmeticException();
        }

    }
}
